use std::env;
use std::process;

use image;
use rqrr;
use qrcode::QrCode;

fn create_qr(text: &str, file: &str) {
    QrCode::new(text)
        .unwrap()
        .render::<image::Luma<u8>>()
        .build()
        .save(file)
        .unwrap();
}

fn read_qr(file: &str) -> String {
    let img = image::open(file).unwrap().to_luma8();

    let mut img = rqrr::PreparedImage::prepare(img);
    let grids = img.detect_grids();
    assert_eq!(grids.len(), 1);

    let (meta, content) = grids[0].decode().unwrap();
    assert_eq!(meta.ecc_level, 0);
    return content;
}

fn print_help() {
    println!("\n**************************************");
    println!("Usage:");
    println!("Program create \"Text to code\" path/to/file");
    println!();
    println!("Example:");
    println!("cargo run create \"My text\" /path/to/result_qr.png");
    println!("\n-------------------------------------\n");
    println!("Program read path/to/file");
    println!();
    println!("Example:");
    println!("cargo run read /path/to/result_qr.png");
    println!("*************************************\n");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args[1] == "create" && args.len() == 4 {
        create_qr(&args[2], &args[3]);
        process::exit(0);
    }

    if args[1] == "read" && args.len() == 3 {
        let result = read_qr(&args[2]);
        println!("Result: {}", result);
        process::exit(0);
    }

    print_help();
}
