# QR-cli

Utility program to create and read [QR](https://en.wikipedia.org/wiki/QR_code) files.

## Installation
> cargo install qr_cli

The executable will be installed in $HOME/.cargo/bin/qr_cli

## Usage

To create a QR from a text run:
> qr_cli create "My text" /path/to/result_qr.png

To read a QR code run:
> qr_cli read /path/to/result_qr.png

An example of usage can be found in *test/generate_and_read.sh*, primarily
used for testing.

## Docker

To create a QR from a text run:
> docker run --rm -it -v "${PWD}":/app jorgarga/qr_cli /qr_cli create "Hello World" /app/test/hello.png

To view the text in a QR image run:
> docker run --rm -it -v "${PWD}":/app jorgarga/qr_cli /qr_cli read /app/test/hello.png