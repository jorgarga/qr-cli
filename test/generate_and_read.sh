#!/bin/bash

# This script exists to test the QR from texts.
#
# A variation of the script could be used to create
# QR from sensible texts (like passwords) while
# avoiding the terminal, so the entered text
# is not registered/saved in any history files.

cargo build --release
./target/release/qr_cli create "This is my text" test.png
./target/release/qr_cli read test.png
